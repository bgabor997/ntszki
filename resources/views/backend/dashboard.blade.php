@extends('backend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Welcome :Name', ['name' => $logged_in_user->name])
        </x-slot>

        <x-slot name="body">
            <div data-vue="true" id="roomc">
                <room-component></room-component>
            </div>
        </x-slot>
    </x-backend.card>
@endsection
