@extends('backend.layouts.app')

@section('title', __('View Announcement'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('View User')
        </x-slot>

        <x-slot name="headerActions">
            <x-utils.link class="card-header-action" :href="route('admin.auth.announcement.index')" :text="__('Back')" />
        </x-slot>

        <x-slot name="body">
            <table class="table table-hover">
                <tr>
                    <th>@lang('Type')</th>
                    <td>{{ $announcement->type }}</td>
                </tr>

                <tr>
                    <th>@lang('Message')</th>
                    <td>{!! $announcement->message !!}</td>
                </tr>

                <tr>
                    <th>@lang('Enabled')</th>
                    <td>@include('backend.auth.announcement.includes.enabled', ['announcement' => $announcement])</td>
                </tr>
            </table>
        </x-slot>

        <x-slot name="footer">
            <small class="float-right text-muted">
                <strong>@lang('Created'):</strong> {{ $announcement->created_at }} ({{ $announcement->created_at->diffForHumans() }}),
                <strong>@lang('Last Updated'):</strong> {{ $announcement->updated_at }} ({{ $announcement->updated_at->diffForHumans() }})
            </small>
        </x-slot>
    </x-backend.card>
@endsection
