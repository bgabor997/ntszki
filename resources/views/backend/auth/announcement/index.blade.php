@extends('backend.layouts.app')

@section('title', __('Announcements'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Announcements')
        </x-slot>

        <x-slot name="headerActions">
            <x-utils.link
                icon="c-icon cil-plus"
                class="card-header-action"
                :href="route('admin.auth.announcement.create')"
                :text="__('Create Announcement')"
            />
        </x-slot>

        <x-slot name="body">
            <livewire:announcements-table />
        </x-slot>
    </x-backend.card>
@endsection
