@if ($announcement->enabled == 1)
    <span class="badge badge-success" data-toggle="tooltip">@lang('Yes')</span>
@else
    <span class="badge badge-danger">@lang('No')</span>
@endif
