<x-utils.view-button :href="route('admin.auth.announcement.show', $announcement)" />
<x-utils.edit-button :href="route('admin.auth.announcement.edit', $announcement)" />
<x-utils.delete-button :href="route('admin.auth.announcement.destroy', $announcement)" />
