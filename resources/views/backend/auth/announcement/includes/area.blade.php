@if ($announcement->area == null)
    <span class="badge badge-info" data-toggle="tooltip">@lang('Everywhere')</span>
@else
    <span class="badge badge-info">@lang($announcement->area)</span>
@endif
