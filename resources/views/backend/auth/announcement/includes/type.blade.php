@inject('user', '\App\Domains\Auth\Models\User')

<span class="badge badge-{{$announcement->type}}" data-toggle="tooltip">@lang($announcement->type)</span>
