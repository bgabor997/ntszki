<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'A jelszava megváltozott!',
    'sent' => 'A jelszó visszaállító linket elküldtük e-mailben!',
    'throttled' => 'Kérem várjon mielött újrapróbálja.',
    'token' => 'A jelszó visszaállító kulcs lejárt.',
    'user' => "Nincs felhasználó ilyen e-mail címmel.",

];
