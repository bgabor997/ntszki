import 'alpinejs'

window.$ = window.jQuery = require('jquery');
window.Swal = require('sweetalert2');

window.Vue = require('vue');
window.vueApps = {};

// CoreUI
require('@coreui/coreui');

// Boilerplate
require('../plugins');

Vue.component('roomComponent', require('./components/RoomComponent.vue').default);

$(document).ready(function () {
    this.vues = $("[data-vue]");
    this.vues.each(function () {
        let id = $(this).attr('id');
        vueApps[id] = new Vue({
            el: '#' + id
        });
    });
});
