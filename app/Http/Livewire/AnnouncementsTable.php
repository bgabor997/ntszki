<?php

namespace App\Http\Livewire;

use App\Domains\Announcement\Models\Announcement;
use App\Domains\Auth\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\TableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class UsersTable.
 */
class AnnouncementsTable extends TableComponent
{
    /**
     * @var string
     */
    public $sortField = 'id';

    /**
     * @var int
     */
    public $enabled;

    /**
     * @param  string  $status
     */
    public function mount($enabled = 1): void
    {
        $this->enabled = $enabled;
    }

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        $query = Announcement::select();

        if ($this->enabled === 1) {
            return $query->where("enabled","=",1);
        }
        else{
            return $query->where("enabled","=",0);
        }
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Type'),"type")
                ->sortable()
                ->view('backend.auth.announcement.includes.type', 'announcement'),
            Column::make(__('Area'),"area")
                ->sortable()
                ->view('backend.auth.announcement.includes.area', 'announcement'),
            Column::make(__('Name'),"name")
                ->sortable()
                ->searchable(),
            Column::make(__('Message'),"message")
                ->html()
                ->searchable(),
            Column::make(__('Enabled'),"enabled")
                ->view('backend.auth.announcement.includes.enabled', 'announcement'),
            Column::make(__('Starts at'),"starts_at")
                ->sortable(),
            Column::make(__('Ends at'),"ends_at")
                ->sortable(),
            Column::make(__('Actions'))
                ->view('backend.auth.announcement.includes.actions', 'announcement'),
        ];
    }
}
